# Optional Dependencies for Neovim

- [telescope] has a couple of optional dependencies.

  1. [fd] is an alternative to `find`.
  2. [ripgrep] is an alternative to `grep`.

  These dependencies can be seen via `:checkhealth`.

[telescope]: https://github.com/nvim-telescope/telescope.nvim
[fd]: https://github.com/sharkdp/fd
[ripgrep]: https://github.com/BurntSushi/ripgrep

<!-- EOF -->
