# Perl and Neovim

My configuration for neovim requires some perl5-modules.

-   I am using [PLS] (instead of [Perl::LanguageServer]).
-   Also, I am using [Neovim::Ext].

These have been installed via [cpanm] (which is in most cases installed by
the operating system's package-manager).

A [useful article] shows how to update local CPAN-modules via `cpanm`.

How to update packages installed via `cpanm`? After everything is installed
according to the article, one may do

```sh
cpan-outdated -p | cpanm
```

[PLS]: https://metacpan.org/pod/PLS
[Perl::LanguageServer]: https://metacpan.org/pod/Perl::LanguageServer
[Neovim::Ext]: https://metacpan.org/pod/Neovim::Ext
[cpanm]: https://metacpan.org/dist/App-cpanminus/view/bin/cpanm
[useful article]:
    https://stackoverflow.com/questions/3727795/how-do-i-update-all-my-cpan-modules-to-their-latest-versions

<!-- EOF -->
