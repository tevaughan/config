# Language-Servers for Neovim


## npm and self-signed certificate

Most (but not all) are most easily installed by way of `npm`.  Behind a
corporate firewall, it is likely that `npm` will fail because of the
self-signed certificate.  This can be fixed by configuring `npm` to look at the
PEM-file corresponding to the company's man in the middle.
```sh
npm config set cafile /path/to/file.pem
```

## yaml-language-server

- Code in nvim-lspconfig: yamlls
- How to install:
```sh
npm i -g yaml-language-server
```

## awk-language-server

- Code in nvim-lspconfig: awk_ls
- How to install:
```sh
npm i -g awk-language-server
```
- Pay attention: When I installed this, the javascript-file was not set as
  executable, and I had to do that manually.  This looks like a bug in npm (or
  in whatever recipe it is following) at the time of my installing
  awk-language-server (version 0.9.4).


## dot-language-server

- Code in nvim-lspconfig: dotls
- How to install:
```sh
npm i -g dot-language-server
```
- Pay attention: This requires node-18 or higher, and so it won't run on
  Ubuntu-18.04, not even with nvm.


## vscode-language-servers

- Codes in nvim-lspconfig: cssls, html, jsonls
- How to install:
```sh
npm i -g vscode-langservers-extracted
```
- Pay attention: This installs multiple executables.


## lua-language-server

- Code in nvim-lspconfig: lua_lsp
- How to install:
```sh
sudo apt install ninja-build
cd ~/Src
git clone --depth=1 https://github.com/sumneko/lua-language-server
cd lua-language-server
git submodule update --depth 1 --init --recursive
cd 3rd/luamake
./compile/install.sh
# If install.sh fail because .bashrc unwritable, then manually add
# 'alias luamake=~/Src/lua-language-server/3rd/luamake/luamake'
# to shell-init.
cd ../..
./3rd/luamake/luamake rebuild
# As final step, somehow put bin/lua-language-server into PATH.
```
- Pay attention: This installs multiple executables.


## typescript-language-server

- Code in nvim-lspconfig: tsserver
- How to install:
```sh
npm i -g typescript-language-server
```

<!-- EOF -->
