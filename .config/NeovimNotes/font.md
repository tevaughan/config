# Font for Terminal

To get the font needed by lualine and other things in neovim, install [Hack
Nerd Font] from the [Nerd-Font site].

On MS Windows, just unpack the zip-file somewhere, and then manually install
each of the fonts labeled "Windows Compatible". Do the installation by
right-clicking on each file and then selecting "Install" from the
context-menu. On Windows 10, that menu-option is immediately visible. On
Windows 11, one has to select "More Options" from the context menu in order
to find "Install".

Then, in the terminal-application, whether it be wsltty, Windows Terminal, or
whatever, tell the application to use "HACK NFM".

[Hack Nerd Font]:
    https://github.com/ryanoasis/nerd-fonts/releases/download/v2.2.2/Hack.zip
[Nerd-Font site]: https://www.nerdfonts.com

<!-- EOF -->
