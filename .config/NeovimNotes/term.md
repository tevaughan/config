# Choice of Terminal for Neovim

Possibly starting with neovim-0.8.1 and certainly displayed in 0.8.2, there
is an annoying bug that is exhibited when neovim runs inside wsltty.

It's not clear to me whether the bug is in neovim, or the bug is in wsltty.

What happens is that sometimes, but not always, when neovim is launched, it
shows some numeric characters at the bottom of the screen in the
command-area, and, when this happens, there is usually one character (usually
a 'g') inserted (prepended to the first line of the file) as the first
character of the file.

This is extremely annoying and does not happen, say, in Windows Terminal.

So I have switched from wsltty to Windows Terminal.

One disadvantage of Windows Terminal, relative to wsltty, is that
xterm-control sequences cannot be used to change the size of the terminal.

But there are other advantages that make up for this.

<!-- EOF -->
