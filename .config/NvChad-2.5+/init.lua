vim.g.base46_cache = vim.fn.stdpath("data") .. "/nvchad/base46/"
vim.g.mapleader = " "

-- This is an attempt to get conform to use stylua.
--
--   - Either conform is not using stylua, or
--   - stylua, as invoked by conform, is not respecting ''~/.stylua.toml'.
--
-- This does not work, but I leave it here as a reminder of the problem.
vim.g.conform_lua = "stylua"

-- Bootstrap lazy and all plugins.
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"

if not vim.loop.fs_stat(lazypath) then
   local repo = "https://github.com/folke/lazy.nvim.git"
   vim.fn.system({
      "git",
      "clone",
      "--filter=blob:none",
      repo,
      "--branch=stable",
      lazypath,
   })
end

vim.opt.rtp:prepend(lazypath)

local lazy_config = require("configs.lazy")

-- load plugins
require("lazy").setup({
   {
      "NvChad/NvChad",
      lazy = false,
      branch = "v2.5",
      import = "nvchad.plugins",
      config = function()
         require("options")
      end,
   },

   { import = "plugins" },
}, lazy_config)

-- load theme
dofile(vim.g.base46_cache .. "defaults")
dofile(vim.g.base46_cache .. "statusline")

require("nvchad.autocmds")

vim.schedule(function()
   require("mappings")
end)

-- Define callback for "VimEnter" or "FileReadPost"..
local function open_nvim_tree(data)
   if vim.fn.isdirectory(data.file) == 1 then
      vim.cmd.cd(data.file)
      local tree = require("nvim-tree.api").tree
      if not tree.is_visible() then
         tree.open()
      end
   end
end

vim.api.nvim_create_autocmd(
   { "VimEnter", "WinEnter", "BufEnter" },
   { callback = open_nvim_tree }
)

-- EOF
