-- This file  needs to have same structure as nvconfig.lua
-- https://github.com/NvChad/NvChad/blob/v2.5/lua/nvconfig.lua

---@type ChadrcConfig
local M = {}

M.base46 = {
   theme = "ayu_dark",

   -- Make each theme, that I like to use, have a purely black background.
   ---@diagnostic disable: missing-fields
   changed_themes = {
      ayu_dark = {
         base_16 = {
            base00 = "#000000",
         },
         base_30 = {
            black = "#000000",
         },
      },
      pastelDark = {
         base_16 = {
            base00 = "#000000",
         },
         base_30 = {
            black = "#000000",
         },
      },
      pastelbeans = {
         base_16 = {
            base00 = "#000000",
         },
         base_30 = {
            black = "#000000",
         },
      },
   },
   ---@diagnostic enable: missing-fields

   -- Force comments to be relatively bright and italicized.
   hl_override = {
      Comment = { italic = true, fg = "#b0d8ff" },
      ["@comment"] = { italic = true, fg = "#b0d8ff" },
   },
}

M.ui = {
   statusline = {
      theme = "vscode_colored",
   },
}

M.mason = {
   cmd = true,
   pkgs = {
      "asm-lsp",
      --
      -- awk-language-server does not install with node-version > 16 on
      -- Windows 10 at work because of the dreaded legacy-renegotiation
      -- problem, apparently related to my company's man in the middle.
      --
      -- The problem happens apparently when node-gyp tries to download an
      -- archive containing the header-files for the current version of node.
      --
      "awk-language-server",
      --
      "bash-debug-adapter",
      "bash-language-server",
      "beautysh",
      "blue",
      "cbfmt",
      --
      -- So long as executable be found, functionality works, and so it is
      -- not necessary for this to be installed by mason.
      --    - Lately, I have been using clangd-17.
      --
      "clangd",
      --
      -- So long as clang-format executable be found, functionality works,
      -- and so it is not necessary for this to be installed by mason.
      --    - I have used sometimes used a python venv in which I was able to
      --      install it via 'pip install clang-format'.
      --    - (Lately, I have been using clang-format-17.)
      --
      "clang-format",
      --
      "cmake-language-server",
      "cmakelang",
      "codelldb", -- for debugging C/C++
      "codespell",
      "commitlint",
      "css-lsp",
      "debugpy",
      "docformatter",
      "dot-language-server",
      "editorconfig-checker",
      --
      -- erb-lint failed on almalinux on 2023-Oct-02 because /usr/bin/ruby
      -- was too old.
      --    - I installed ruby-3.2.2 via rbenv to fix it.
      --
      "erb-lint",
      --
      "html-lsp",
      "latexindent",
      "lua-language-server",
      "marksman",
      "matlab-language-server",
      "opencl-language-server",
      "perlnavigator",
      "powershell-editor-services",
      "prettier",
      "proselint",
      --
      -- 'python-lsp-server' did not install at work via mason.  But I do
      -- have a venv in which I was able to install it via 'pip install
      -- python-lsp-server'.  So the executable is in my path.
      --
      "python-lsp-server",
      --
      -- ruby-lsp failed on almalinux on 2023-Oct-02 because /usr/bin/ruby is
      -- too old. I installed ruby-3.2.2 via rbenv to fix it.
      --
      "ruby-lsp",
      --
      "rust-analyzer",
      --
      -- 'shfmt' formats a shell-script.
      "shfmt",
      --
      "stylua",
      --
      -- Mason's texlab fails to run on almalinux-8 because its glibc is too
      -- old.
      --
      --   One can install texlab via
      --   'cargo install --git https://github.com/latex-lsp/texlab'.
      --
      --"texlab", -- LSP for LaTeX
      --
      -- Mason's tree-sitter-cli fails to run on almalinux-8 because its
      -- glibc is too old.
      --
      --   One can install tree-sitter-cli via
      --   'cargo install tree-sitter-cli'
      --
      --"tree-sitter-cli",
      --
      "typescript-language-server", -- typescript and javascript
      "vim-language-server",
      "yaml-language-server",
   },
}

return M

-- EOF
