require("nvchad.options")

local opt = vim.opt

-- conceallevel=0 means not to conceal anything.
opt.conceallevel = 0

-- Show? some invisible characters.
opt.list = true

-- Set number of columns before hard wrap, and color subsequent column gray.
opt.textwidth = 77
opt.colorcolumn = "+1"
vim.cmd("highlight ColorColumn guibg=gray")

-- undofile is false (turned off) here because I like to be able to hit undo
-- a bunch of times to get back to how the file was when I opened it.
--
--    - No matter how many times I hit 'u', I do *not* want the file to
--      revert to a state *before* what it was when I opened it.
--
opt.undofile = false

-- Do not mention 'h' and 'l', so that such horizontal motion does not wrap
-- past line-boundary.
opt.whichwrap = "<,>,[],b,s"

-- Disable soft wrap.
opt.wrap = false

-- Set the title of the terminal.
vim.o.title = true
vim.o.titlestring = "%F"

-- My autocommand-configuration group.
local group_id = vim.api.nvim_create_augroup("MyCfgGroup", { clear = true })

-- Set the text-width to gitjournal_tw for markdown-buffer whose absolute
-- path contains "notes-github.com".
--
--   - I do this because notes are sometimes edited and viewed on my
--     phone in the GitJournal app.
--
--   - When the phone is vertical, I have only about 44 columns of text
--     available in the "raw" editor in GitJournal, and that's with the
--     text-size set so small as it can go.
--
local gitjournal_tw = 44

-- For a markdown-buffer accessible via gitjournal, set the textwidth to
-- `gitjournal_tw` by way of autocmd on entering buffer.
--
--   - At first I tried using "FileType" as the first argument to
--     nvim_create_autocmd() (and setting pattern to "markdown").
--
--   - That worked only when naming the file on nvim's command-line at launch
--     and did not work naming the file as an argument to ':split'.
--
vim.api.nvim_create_autocmd({ "BufEnter", "BufWinEnter" }, {
   group = group_id,
   pattern = "*.md",
   callback = function()
      local abs_path = vim.fn.fnamemodify(vim.fn.expand("%"), ":p")
      -- Escape the dash with '%'; dash is special for 'string.find()'.
      if string.find(abs_path, "Src/notes%-github.com") then
         vim.api.nvim_set_option_value(
            "textwidth",
            gitjournal_tw,
            { scope = "local" }
         )
      end
   end,
})

-- Setting 'vim.opt.indentexpr = ""', in the callback defined in the autocmd
-- just above, fails to make 'indentexpr' be set to the empty string in a
-- markdown-buffer.
--
-- Perhaps treesitter is setting 'indentexpr' after the call back clears it.
--
-- So disable treesitter's handling of indentation for markdown altogether.
--
-- This seems to be necessary so that 'gq' works in normal mode to format a
-- long item in a bulleted list, etc.
--
---@diagnostic disable-next-line: missing-fields
require("nvim-treesitter.configs").setup({
   indent = {
      enable = true,
      disable = { "markdown" },
   },
})

-- By default, neovim seems to force the recommended style, which includes
-- shiftwidth=4, for markdown.
--
--    - I turn off the enforcement here so that I can use editorconfig.
--
vim.g.markdown_recommended_style = false

-- EOF
