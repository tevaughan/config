-- This configuration seems to work for cpp but not for lua.
--
--   - Either stylua is not actually being invoked by conform, or else
--   - stylua is being invoked but is not honoring ~/.stylua.toml.
--
-- stylua seems to respect ~/.stylua.toml when stylua is invoked on the
-- command-line.

local options = {
   formatters_by_ft = {
      c = { "clang-format" },
      cmake = { "cmake-format" },
      cpp = { "clang-format" },
      css = { "prettier" },
      html = { "prettier" },
      lua = { "stylua" },
      markdown = { "prettier" },
      python = { "blue" },
      rust = { "rustfmt" },
      sh = { "shfmt" },
   },

   format_on_save = {
      -- These options will be passed to conform.format().
      timeout_ms = 500,
      lsp_fallback = false,
   },

   formatters = {
      shfmt = {
         command = "shfmt",
         -- By default shfmt uses TAB for indentation.
         -- 'shfmt -i 3' uses three spaces rather than TAB for indentation.
         args = { "-i", "3" },
      },
      ["cmake-format"] = {
         command = "cmake-format",
         args = { "-" },
      },
   },
}

require("conform").setup(options)

-- EOF
