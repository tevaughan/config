local configs = require("nvchad.configs.lspconfig")

local on_attach = configs.on_attach
local on_init = configs.on_init
local capabilities = configs.capabilities

local lspconfig = require("lspconfig")

-- See
-- github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md
-- for list of server-names as known by lspconfig.  Each of these can be
-- slightly different from the name that the server calls itself!
local servers = {
   "asm_lsp",
   "awk_ls",
   "bashls",
   "clangd",
   "cmake",
   "cssls",
   "dotls",
   "html",
   "lua_ls",
   "marksman",
   "matlab_ls",
   "opencl_ls",
   "perlnavigator",
   "powershell_es",
   "ruby_lsp",
   "rust_analyzer",
   "texlab",
   "ts_ls",
   "vimls",
   "yamlls",
}

-- Default config for each server.
for _, lsp in ipairs(servers) do
   lspconfig[lsp].setup({
      on_attach = on_attach,
      on_init = on_init,
      capabilities = capabilities,
   })
end

-- This overwrites, only for lua_ls, what was done in loop above.
lspconfig.lua_ls.setup({
   -- Stop lua-language-server from complaining about unknown global
   -- variable, 'vim'.
   settings = {
      Lua = {
         runtime = {
            -- Tell language-server which version of Lua is running.  This is
            -- most likely LuaJIT in case of Neovim.
            version = "LuaJIT",
         },
         diagnostics = {
            --
            -- Don't complain about these words in neovim's configuration. This
            -- seems not to be necessary if 'workspace.library' be defined
            -- below.
            --
            --globals = { 'vim', 'require', },
         },
         --
         -- This takes a long time to load.
         -- But maybe it's worth the wait.
         --
         workspace = {
            -- Make server aware of Neovim runtime files.
            library = vim.api.nvim_get_runtime_file("", true),
         },
         -- Do not send telemetry containing a randomized, unique identifier.
         telemetry = { enable = false },
      },
   },
   on_attach = on_attach,
   capabilities = capabilities,
})

-- EOF
