-- Assigning to 'opts' key here will extend the default 'opts'.

-- cmp is used, in configuration of mkdnflow below, to add mkdnflow as a
-- source of completion for nvim-cmp.
--   - Because of a circularity during the initial run of nvim on a new
--     system, cmp_status will be false on that initial run, and so mkdnflow
--     will not on neovim's first run be added as a source of completion for
--     nvim-cmp.
--   - But mkdnflow will be added as a source of completion on neovim's
--     second and subsequent invocations on the system.
local cmp_status, cmp = pcall(require, "nvchad.configs.cmp")

return {
   --
   --------------------------------------------------------------------------
   -- Disable autopairs, which is enabled by default in nvchad.
   {
      "windwp/nvim-autopairs",
      lazy = true,
      config = function()
         -- Disable autopairs by not calling setup.
      end,
   },
   --
   --------------------------------------------------------------------------
   -- cmake-tools.nvim assists with cmake.
   {
      "Civitasv/cmake-tools.nvim",
      lazy = false,
      config = function()
         require("cmake-tools").setup({})
      end,
   },
   --
   --------------------------------------------------------------------------
   -- Codeium is commented out because it copies local code, which might be
   -- copyrighted, up to codeium's server.
   -- {
   --    "Exafunction/codeium.vim",
   --    event = "BufEnter",
   --    config = function()
   --       -- Change binding for accepting suggestion.  Default is <TAB>.
   --       vim.keymap.set("i", "<C-g>", function()
   --          return vim.fn["codeium#Accept"]()
   --       end, { expr = true, silent = true })
   --    end,
   -- },
   --
   --------------------------------------------------------------------------
   -- startify provides a nice start-screen if neovim be not given, via the
   -- command-line, the name of a file to edit.
   { "mhinz/vim-startify", lazy = false },
   --
   --------------------------------------------------------------------------
   -- mkdnflow is a project to bring a vimwiki-style plugin to neovim via
   -- lua.
   --
   --    - I have gone back and forth between using this and using
   --      telekasten.
   --
   --    - See https://github.com/jakewvincent/mkdnflow.nvim?tab=readme-ov-file#%EF%B8%8F-configuration
   --      - I have configured mdnflow below to expect that each notebook has
   --        'index.md' at its root.
   {
      "jakewvincent/mkdnflow.nvim",
      lazy = false,
      config = function()
         require("mkdnflow").setup({})
         -- This is my attempt to follow the suggestion under 'Completion for
         -- nvim-cmp' at 'https://github.com/jakewvincent/mkdnflow.nvim'.
         if cmp_status then
            table.insert(cmp.sources, { name = "mkdnflow" })
         end
      end,
   },
   --
   --------------------------------------------------------------------------
   -- My configuration of colorizer turns OFF by default the coloring of a
   -- color-name, whenever it appears in a buffer of any kind.
   {
      "NvChad/nvim-colorizer.lua",
      opts = {
         user_default_options = {
            names = false,
         },
      },
   },
   --
   --------------------------------------------------------------------------
   -- If event be set to BufWritePre, then format the buffer before saving
   -- its contents to the file-system.
   {
      "stevearc/conform.nvim",
      event = "BufWritePre", -- format on save
      config = function()
         require("configs.conform")
      end,
   },
   --
   --------------------------------------------------------------------------
   {
      "neovim/nvim-lspconfig",
      config = function()
         require("nvchad.configs.lspconfig").defaults()
         require("configs.lspconfig")
      end,
   },
   --
   --------------------------------------------------------------------------
   -- Install many a convenient executable for neovim to make use of.
   { "williamboman/mason.nvim" },
   --
   --------------------------------------------------------------------------
   {
      "nvim-treesitter/nvim-treesitter",
      opts = {
         ensure_installed = {
            "awk",
            "bash",
            "bibtex",
            "c",
            "cmake",
            "cpp",
            "css",
            "diff",
            "dockerfile",
            "dot",
            "fortran",
            "git_config",
            "git_rebase",
            "gitattributes",
            "gitcommit",
            "gitignore",
            "html",
            "ini",
            "json",
            "latex",
            "lua",
            "luadoc",
            "make",
            "markdown",
            "matlab",
            "ninja",
            "norg",
            "objc",
            "org",
            "passwd",
            "pem",
            "perl",
            "python",
            "r",
            "regex",
            "rst",
            "ruby",
            "rust",
            "toml",
            "typescript",
            "vim",
            "vimdoc",
            "yaml",
         },
      },
   },
   --
}

-- EOF
