

# Make a nice, colored command-prompt.
function prompt {
  $myPsDebugContext    = $ExecutionContext.SessionState.PSDebugContext
  $myNestedPromptLevel = $ExecutionContext.SessionState.NestedPromptLevel
  $esc                 = [char]27

  "$esc[48;2;160;255;160;30m " +
  $(if ($myPsDebugContext) { '[DBG]: ' } else { '' }) +
  'PS ' + $(Get-Location) + " $ESC[0m " +
  $(if ($myNestedPromptLevel -ge 1) { '>>' }) +
  '> '
}


# For function-ls on MS Windows, get a filename's suffix on the basis of the
# file's type.
function Get-Suffix {
  param (
    [System.IO.FileSystemInfo]$item
  )

  if ($item.Attributes -band [System.IO.FileAttributes]::Directory) {
    return "/"
  } elseif ($item.Attributes -band [System.IO.FileAttributes]::ReparsePoint) {
    return "@"
  } else {
    return ""
  }
}


# Make vim be an alias for nvim.
function vim { nvim @args }


# Define grep as a function.
if (Get-Command grep -ErrorAction SilentlyContinue) {
  # Just call system's grep-program.
  function grep { /usr/bin/grep @args }
} else {
  # Roughly simulate behavior of unix's grep-program.
  function grep {
    [CmdletBinding()]
    param (
      [string]$pattern,
      [string[]]$path,
      [switch]$l,
      [switch]$r,
      [switch]$v
    )

    $cmdArgs = @{
      Pattern = $pattern
    }

    $didRecursion    = $false
    $uniqueFilenames = @{}

    if ($path) {
      # At least one path to search is specified.
      $files = $path | ForEach-Object {
        if (Test-Path $_ -PathType Container) {
          # Current element of path is directory.
          if ($r) {
            # Recursively search the path.
            $didRecursion = $true
            Get-ChildItem -Path $_ -Recurse
              | Where-Object { -not $_.PSIsContainer }
              | ForEach-Object { $_.FullName }
          } else {
            Write-Error "Error: $_ is a directory.  Use '-r' for recursion."
          }
        } else {
          # Current element of path is file.
          $_
        }
      }
      $cmdArgs['Path'] = $files
    } else {
      # Not even one path to search is specified.
      if ($r) {
        # Recursively search the current directory.
        $didRecursion = $true
        $cmdArgs['Path'] = Get-ChildItem -Recurse
          | Where-Object { -not $_.PSIsContainer }
          | ForEach-Object { $_.FullName }
      } else {
        Write-Error "Error: No path specified.  Use '-r' for recursion."
      }
    }

    if ($v) { $cmdArgs['NotMatch'] = $true }

    $output = Select-String @cmdArgs

    if ($l) {
      $output = $output | ForEach-Object {
        $filename = $_.Path | Resolve-Path -Relative
        if (-not $uniqueFilenames.ContainsKey($filename)) {
          $uniqueFilenames[$filename] = $true
          $filename
        }
      }
    }

    if (($r) -and (-not $didRecursion))
    {
      Write-Warning "Warning: '-r' specified but *no* directory searched"
      Write-Warning "Warning: possibly '*' specified as path"
    }

    $output
  }
}


# Make sure that there is no existing alias for ls, for one is defined below.
if (Test-Path alias:\ls) {
  Remove-Item Alias:\ls
}


# Define ls as a function.
if (Get-Command ls -ErrorAction SilentlyContinue) {
  # Just call system's ls-program with flag to append suffix to each file.
  function ls { /bin/ls -F @args }
} else {
  # Roughly simulate behavior of unix's ls-program.
  function ls {
    param (
      [switch]$a,
      [switch]$l,
      [switch]$al,
      [switch]$la
    )

    if ($l) {
      if ($a) {
        Get-ChildItem -Force @args
      } else {
        Get-ChildItem @args
      }
    } elseif ($al -or $la) {
      Get-ChildItem -Force @args
    } else {

      # In construction of $items, @() ensures that $items is always an array.
      #
      # - Without @(), $items will be array of strings when Get-ChildItem have
      #   more than one name to return.
      #
      # - However, $items will just be string if Get-ChildItem return only
      #   single name.
      if ($a) {
        $items = @(Get-ChildItem -Force @args)
      } else {
        $items = @(Get-ChildItem @args)
      }

      # Initialize as empty the list of file-name-strings, each possibly quoted
      # and possibly appended with a character indicating the file's type (such
      # as directory or symbolic link).
      $names = @()

      # Longest length of any file-name-string in $names.
      $maxNameLen = 0

      # Populate $names, and calculate $maxNameLen.
      for ($i = 0; $i -lt $items.Length; $i++) {
        $item = $items[$i]
        $name = $item.Name
        if ($name -like "* *") { $name = "`"$($name)`"" }
        $name += Get-Suffix $item
        $names += $name
        if ($name.Length -gt $maxNameLen) { $maxNameLen = $name.Length }
      }

      # - Generate so many columns as will fit in terminal.
      # - Print every line that has all columns filled.
      $output = ""
      $width = $Host.UI.RawUI.WindowSize.Width
      for ($i = 0; $i -lt $names.Length; $i++) {
        $name = $names[$i]
        $output += $name
        $padToNextCol = $maxNameLen - $name.Length + 2
        if(($output.Length + $padToNextCol + $maxNameLen) -ge $width) {
          Write-Host $output
          $output = ""
        } else {
          $output += ' ' * $padToNextCol
        }
      }

      # If last line, which has not all columns filled, be not empty, then
      # print it.
      if ($output.Length -gt 0) { Write-Host $output }
    }
  }
}


# EOF
