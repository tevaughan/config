
# powershell

The present directory contains [Microsoft.PowerShell_profile.ps1],
which is executed whenever PowerShell's executable, `powershell.exe` or
`pwsh.exe`, is launched.

[Microsoft.PowerShell_profile.ps1]: Microsoft.PowerShell_profile.ps1

- In PowerShell, the variable, `$PROFILE`, contains the pathname of the
  profile that the shell reads.

- On a unix-like operating system, `$PROFILE` contains
  ```
  ~/.config/powershell/Microsoft.PowerShell_profile.ps1
  ```

- On Microsoft Windows, the profile sits under the user's folder,
  `Documents`.
  - `$PROFILE` might contain, for example,
    ```ps1
    $HOME\OneDrive\Documents\PowerShell\Microsoft.PowerShell_profile.ps1
    ```

- On either operating system, one might make `$PROFILE` be a symbolic link
  that points to [Microsoft.PowerShell_profile.ps1] here.
  - Suppose that `$prof` contains the absolute path to the local copy of
    [Microsoft.PowerShell_profile.ps1].
  - Suppose further that the directory-portion of the pathname in `$PROFILE`
    exists but that the file pointed to by `$PROFILE` does not exist.
  - Then
    ```ps1
    New-Item -ItemType SymbolicLink -Target $prof -Path $PROFILE
    ```

<!-- EOF -->
