# NvChad

`NvChad-2.5+` is not meant to be pointed to by the corresponding directory
under `~/.config` but is here in order to make explicit that the
configuration is only for nvim that is set up to run at least Version 2.5 of
[NvChad][NvChad].

[NvChad]: https://github.com/NvChad

<!-- EOF -->
