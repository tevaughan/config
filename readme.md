# config

Here are some of the files that I use to configure my environment.

Most of the files here are named exactly as they would be if the top-level
directory here were the home-directory of my account.

-   In my home-directory, any file or directory that I want to reflect the
    repository's configuration is a logical link that points into the local
    copy of this repository.
