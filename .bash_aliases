# vim: set filetype=sh:
#
# This file, residing in a local copy of
# 'https://gitlab.com/tevaughan/config', should be pointed to by a symlink,
# '~/.bash_aliases'.
#
# This file contains not just aliases but also other bashrc-type code
# appropriate only for my generic configuration, which is common to all of my
# shells, regardless of the particularities of the machine.
#
# The overall idea is that '~/.bashrc' should be a symlink to
# '/etc/skel/.bashrc', so that it is up to date with the operating system's
# idea of how the shell should be initialized.  At least on Debian-like
# systems, that idea presently involves sourcing '~/.bash_aliases' near the
# bottom of '/etc/skel/.bashrc'.  I use that as my hook to customize the
# configuration of the shell.
#
# Each of my specific configurations for a particular machine should be
# pointed to by a symlink matching '~/.config/bash_aliases*', so that it will
# be sourced by '~/.bash_aliases'.

fname="$(printf '%30s' $(basename ${BASH_SOURCE[0]}))"
echo "${fname}: Vaughan's generic config"

export PATH

getShortPath() {
   foo="$1"
   shortened_path="${foo/#$HOME/~}"
   if [[ "$foo" == "$HOME"* ]]; then
      shortened_path="~${foo#$HOME}"
   fi
   echo $shortened_path
}

addToFrontOfPath() {
   local new_element="$1" # New element to add.
   local fname="$2"       # Name of file calling addToFrontOfPath.
   local short_path=$(getShortPath "${new_element}")
   if echo $PATH | grep -E "^(.*:)?${new_element}(:.*)?$" >/dev/null; then
      #echo "${fname}: Already in PATH: '${short_path}'."
      return
   fi
   if ! test -d "${new_element}"; then
      echo "${fname}: Not a directory: '${short_path}'."
      return
   fi
   echo "${fname}: Adding '${short_path}' to PATH."
   PATH="${new_element}:${PATH}"
}

pwsh="/mnt/c/Program Files/PowerShell/7/pwsh.exe"
if [ -x "${pwsh}" ] && which wslpath >/dev/null 2>&1; then
   cmd="[System.Environment]::GetEnvironmentVariable('Path', 'Machine')"
   win_path=$("$pwsh" -Command "${cmd}" | sed 's,\\,/,g' | tr -d '\n\r')
   echo "${win_path}" | sed 's/;/\n/g' >/tmp/win_paths.txt
   while IFS= read -r path; do
      new_element=$(wslpath -u "${path}")
      addToFrontOfPath "${new_element}" "${fname}"
   done </tmp/win_paths.txt
fi

addToFrontOfPath "/usr/local/bin" "${fname}"

addToFrontOfPath "${HOME}/node_modules/.bin" "${fname}"

addToFrontOfPath "${HOME}/.local/bin" "${fname}"
addToFrontOfPath "${HOME}/.local/share/nvim/mason/bin" "${fname}"
addToFrontOfPath "${HOME}/Bin" "${fname}"

# Make sure that cargo's binaries are in PATH.
if ! echo $PATH | grep "${HOME}/.cargo/bin" >/dev/null; then
   if [ -r "${HOME}/.cargo/env" ]; then
      echo "${fname}: Sourcing '$HOME/.cargo/env'."
      source "${HOME}/.cargo/env"
   else
      addToFrontOfPath "${HOME}/.cargo/bin" "${fname}"
   fi
fi

alias ls='/bin/ls --color'
alias la='/bin/ls --color -Ahs'
alias ll='/bin/ls --color -Aho'

# Change terminal's title, and print a status-line before PS1 is printed.
__prompt_command() {
   # Error-code `e` needs to be stored first.
   local e="$?"

   # Maximum number of characters to print on line before prompt.
   # Allow five characters for printing error-code.
   local t=$(($(tput cols) - 5))

   # If discernible, then put name of OS into terminal's title.
   #
   # - Override /etc/os-release by uname in case of MINGW64, in orderto
   #   distinguish mingw64-shell from msys-shell.
   local RELFILE=/etc/os-release
   if [ -r ${RELFILE} ]; then
      os=$(
         . $RELFILE
         echo $NAME $VERSION
      )
      echo -ne "\033]0;${os}\007"
   fi
   if uname | grep MINGW64 >/dev/null; then
      echo -ne "\033]0;MINGW64\007"
   fi

   # Store in `cwd` up to n characters at end of PWD.
   local n_chars=$((t - 1))
   if ((${#PWD} < n_chars)); then n_chars=${#PWD}; fi
   local negative_n_chars="-$n_chars"
   local cwd="${PWD:negative_n_chars}"

   # Print line before prompt.
   if (($(tput colors) > 7)); then
      tput setaf 2
      tput setab 0
   fi
   tput rev
   echo -n "$cwd"
   tput sgr0
   # Possibly print error-code, and terminate line before prompt.
   if [ $e != 0 ]; then
      tput setaf 1
      echo " $e"
      tput sgr0 # Print return-code in red.
   else
      echo ""
   fi
}

export EDITOR=$(if ! which nvim 2>/dev/null; then which vi; fi)
export PROMPT_COMMAND=__prompt_command
export PS1="\u@\h \$ "

vim() {
   local nv=$(which nvim 2>/dev/null)
   if [ "x$nv" != "x" ]; then
      "$nv" $*
   elif [ -x /usr/bin/vim ]; then
      /usr/bin/vim $*
   else
      echo "${fname}: ERROR: neither nvim nor vim found in path"
   fi
}

# Make the sort-command use the traditional (C) ordering, except on MINGW64,
# which seems to need en_US instead of C.
if uname | grep MINGW64 >/dev/null; then
   export LANG=en_US.UTF-8
   export LC_ALL=en_US.UTF-8
   export LC_CTYPE=en_US.UTF-8
elif locale -a | grep C.UTF-8 >/dev/null; then
   # Ubuntu-18.04 supports C.UTF-8 but not C.utf8.
   export LANG=C.UTF-8
   export LC_ALL=C.UTF-8
   export LC_CTYPE=C.UTF-8
elif $(locale -a | grep C.utf8 >/dev/null); then
   export LANG=C.utf8
   export LC_ALL=C.utf8
   export LC_CTYPE=C.utf8
else
   export LANG=C
   export LC_ALL=C
   export LC_CTYPE=C
fi

# Handle configuration and notes.
config() {
   for d in ~/Src/config-* ~/Src/notes-*; do
      if ! [ -d "$d" ]; then continue; fi
      pushd "$d" >/dev/null
      printf '\n'
      printf '=%.0s' $(seq ${#d})
      printf '\n'
      echo "${d}"
      printf '=%.0s' $(seq ${#d})
      printf '\n\n'
      git $*
      popd >/dev/null
   done
}

for a in ~/.config/bash_aliases*; do
   if [ -r "$a" ]; then . "$a"; fi
done

# EOF
